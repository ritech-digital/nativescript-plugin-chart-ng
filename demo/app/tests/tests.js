var NativescriptPluginChartsNg = require("nativescript-nativescript-plugin-charts-ng").NativescriptPluginChartsNg;
var nativescriptPluginChartsNg = new NativescriptPluginChartsNg();

// TODO replace 'functionname' with an acual function name of your plugin class and run with 'npm test <platform>'
describe("functionname", function() {
  it("exists", function() {
    expect(nativescriptPluginChartsNg.functionname).toBeDefined();
  });

  it("returns a promise", function() {
    expect(nativescriptPluginChartsNg.functionname()).toEqual(jasmine.any(Promise));
  });
});