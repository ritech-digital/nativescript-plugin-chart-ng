import {Observable} from 'data/observable';
import {NativescriptPluginChartsNg} from 'nativescript-nativescript-plugin-charts-ng';

export class HelloWorldModel extends Observable {
  public message: string;
  private nativescriptPluginChartsNg: NativescriptPluginChartsNg;

  constructor() {
    super();

    this.nativescriptPluginChartsNg = new NativescriptPluginChartsNg();
    this.message = this.nativescriptPluginChartsNg.message;
  }
}